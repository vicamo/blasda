#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import setuptools

with open("README.md", "r") as fh:
    readme = fh.read()

setuptools.setup(
    name = "blasda",
    author = "You-Sheng Yang",
    author_email = "vicamo@gmail.com",
    description = ("Bluetooth LE Advertisement Scanner by dasbus."),
    long_description = readme,
    long_description_content_type = "text/markdown",
    keywords = ['bluetooth', 'advertising', 'ble', 'bluez', 'dasbus'],
    url = "https://gitlab.com/vicamo/blasda",
    packages = setuptools.find_packages(),
    python_requires = '>=3',
    install_requires = ['dasbus'],
    classifiers = [
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Topic :: Utilities",
    ],
)
