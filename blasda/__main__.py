#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import argparse

arg_parser = argparse.ArgumentParser(
    description="Bluetooth LE Advertisement Scanner by dasbus")
try:
    opts = arg_parser.parse_args()
except Exception as e:
    arg_parser.error("Error: " + str(e))
    sys.exit()
